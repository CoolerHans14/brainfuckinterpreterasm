; Brainfuck Interpreter written in nasm by kroettenotter
; Input from brainfuck code via stdin

global _start
section .text

_start: 
        ;print startUpBanner
        mov rax, 1
        mov rdi, 1
        mov rsi, startUpBanner
        mov rdx, 40
        syscall

        ;get brainfuck code
        mov rax, 0
        mov rdi, 0
        mov rsi, input
        mov rdx, 512
        syscall
       
        jmp init


check:  ; checks the current diggit in the code
	mov r9b, [input+r8]
	mov r11b, '<'
	cmp r11b, r9b
	je caseBackward
	mov r11b, '>'
	cmp r11b, r9b
	je caseForward
	mov r11b, '+'
	cmp r11b, r9b
	je caseIncrement
	mov r11b, '-'
	cmp r11b, r9b
	je caseDecrement
	mov r11b, '.'
	cmp r11b, r9b
	je caseOutput
	mov r11b, ','
	cmp r11b, r9b
	je caseInput
	mov r11b, '['
	cmp r11b, r9b
	je caseLoopBegin
	mov r11b, ']'
	cmp r11b, r9b
	je caseLoopEnd
        jmp end
        
init:
	cmp r11, 0
	je finishInit
	jmp pushToStack
	
finishInit:
	mov r8, 0
	add rsp, 64
	jmp check
	
pushToStack:
	dec r11
	push 0
	jmp init

caseForward: ;if current charektar is >
        inc r8          
        sub rsp, 8      ;go 1 address (64bit) Forward
        jmp check
        
caseBackward: ;if current character is <
        inc r8
        add rsp, 8      ;go 1 address (64bit) Backward
        jmp check
        
caseIncrement: ;if current character is +
        inc r8
        pop r10
        inc r10
        push r10
        jmp check
        
caseDecrement: ;if current character is -
        inc r8
        pop r10
        dec r10
        push r10
        jmp check
        
caseOutput: ;if current character is .
        inc r8
        mov rax, 1
        mov rdi, 1
        mov rsi, rsp
        mov rdx, 1
        syscall
        jmp check
        
caseInput: ;if current character is ,
	 inc r8
        mov rax, 0
        mov rdi, 0
        mov rsi, rsp
        mov rdx, 1
        syscall
        jmp check
        
caseLoopBegin: ;if current character is [
	mov rcx, 0
	inc r8
	cmp rsp, 0
	je  findEndOfLoop
	jmp check
	
caseLoopEnd: ;if current character is ]
	mov rcx, 0
	inc r8
	jmp  findBeginOfLoop
	
findBeginOfLoop:
	jmp check
	mov rax, '['
	cmp rax, [input+r8]
	je foundOpenBricket
	mov rax, ']'
	cmp rax, [input+r8]
	je foundCloseBricket
	dec r8
	jmp findBeginOfLoop
	
foundCloseBricket:
	inc rcx
	dec r8
	jmp findBeginOfLoop
	
foundOpenBricket:  
	cmp rcx, 0
	je  check
	dec rcx
	dec r8
	jmp findBeginOfLoop
	
findEndOfLoop:
	mov rax, '['
	cmp rax, [input+r8]
	je foundLoopBeginInLoop
	mov rax, ']'
	cmp rax, [input+r8]
	je foundLoopEndInLoop
	inc r8
	jmp findEndOfLoop
	
goForwardInLoop:
	inc r8
	jmp findEndOfLoop
	
decrementLoopCount:
	dec rcx
	jmp findEndOfLoop
	
foundLoopBeginInLoop:
	inc r8
	inc rcx
	jmp findEndOfLoop
	
foundLoopEndInLoop:
	cmp rcx, 0
	inc r8
	jne decrementLoopCount
	je check

end:    ;exit the programm
        mov rax, 60
        mov rdi, 0
        syscall

    

section .data

startUpBanner: db "Write the brainfuck code and press enter",40
input: dw "",512
hier: db "hier",4
